﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;

namespace Extensions {
	public static class OtherExtensions {
		public static void PropChanged(this INotifyPropertyChanged source, [CallerMemberName] string name = null, object obj = null) {
			Type type = source.GetType();
			while (type.BaseType.IsAssignableTo(typeof(INotifyPropertyChanged))) {
				type = type.BaseType;
			}
			var field = type.GetField("PropertyChanged", BindingFlags.Instance | BindingFlags.NonPublic);
			var eventDelegate = (PropertyChangedEventHandler) field.GetValue(source);
			eventDelegate?.Invoke(obj ?? source, new PropertyChangedEventArgs(name));
		}
	}

	public static class IntExtensions {
		public static void Times(this int count, Action action) {
			for (int i = 0; i < count; i++) action();
		}

		public static void Times(this int count, Action<int> action) {
			for (int i = 0; i < count; i++) action(i);
		}
	}

	public static class StringExtensions {
		public static string Join(this string sep, IEnumerable<object> list) {
			return string.Join(sep, list);
		}
	}

	public static class LinqExtensions {
		public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action) {
			foreach (var e in enumerable) {
				action(e);
			}
		}

		public static void ForEach<T>(this IEnumerable<T> enumerable, Action<int, T> action) {
			var i = 0;
			foreach (var e in enumerable) {
				action(i++, e);
			}
		}

		public static bool In<T>(this T obj, IEnumerable<T> seq) => 
			obj switch {
				IEnumerable<T> e => seq.All(s => e.Contains(s)), // overhead when used like !a.In(...)
				ITuple tuple => obj.In(Enumerable.Range(0, tuple.Length - 1).Select(i => tuple[i])),
				_ => seq.Contains(obj)
			};

		// TODO Not safe
		public static IEnumerable<T> SortLike<T, L>(this IEnumerable<T> enumerable,
				IEnumerable<L> like, Func<T, L> selector) {
			var list = like.ToList();
			return enumerable.OrderBy(e => list.IndexOf(selector(e)));
		}

		public static int IndexOf<T>(this IEnumerable<T> enumerable, T element) {
			switch (enumerable) {
				case ICollection<T> c:
					return c.IndexOf(element);
				default:
					var i = 0;
					foreach (var e in enumerable) {
						if (e.Equals(element))
							return i;
						i++;
					}
					return -1;
			}
		}
	}
}

