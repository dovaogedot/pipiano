﻿using Pipiano.IO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipiano.Core {
	public class Keyboard {
		public IReadOnlyDictionary<int, Key> Keys => _keys;
		public event Action<Key> KeyStateChanged;
		public IMidiListener MidiListener {
			get => _midiListener;
			set {
				if (_midiListener is not null) {
					_midiListener.KeyPressed -= OnMidiListener_KeyPressed;
				}
				_midiListener = value;
				_midiListener.KeyPressed += OnMidiListener_KeyPressed;
			}
		}

		private Dictionary<int, Key> _keys = new();
		private IMidiListener _midiListener;

		public Keyboard(int keyCount, int startingindex = 0) {
			for (int i = 0; i < keyCount; i++) {
				_keys.Add(i + startingindex, new Key(i + startingindex));
			}
		}

		private void OnMidiListener_KeyPressed(KeyPressedEvent keyPressedEvent) {   
			var key = _keys[keyPressedEvent.Note];
			key.Strength = keyPressedEvent.Strength;
			KeyStateChanged?.Invoke(key);
			//Debug.WriteLine(key);
		}
	}
}
