﻿using Extensions;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipiano.Core {
	public record Note {
		public string Name => _name;
		public int Octave => _octave;
		public Scale Scale;
		public int Index => _index;

		private string _name;
		private int _octave;
		private int _index;

		public Note(int index, Scale scale = null) {
			_index = index;
			Scale = scale ?? Scale.CMajor;

			_octave = index / 12 - 2;

			var pitch = index % 12;
			_name = Scale[pitch];
		}

		public Note(Note note) {
			_name = note._name;
			_octave = note._octave;
			Scale = note.Scale;
		}

		public Note(Key key, Scale scale = null) : this(key.Index) { }
		
		public Note Shift(int octaves) => this with { _octave = _octave + octaves };

		public override string ToString() {
			return Name;
		}
	}
}
