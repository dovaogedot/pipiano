﻿using Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipiano.Core {
	public class Key {
		public int Index { get; }
		public int Strength { get; set; }
		public int Octave => _octave;
		public KeyColor Color => _color;

		private int _octave;
		private KeyColor _color;

		private static int[] _blacks = new int[] { 1, 3, 6, 8, 10 };

		public Key(int index, int strength = 0) {
			Index = index;
			Strength = strength;
			_octave = index / 12;
			_color = (index % 12).In(_blacks) ? KeyColor.Black : KeyColor.White;
		}

		public override string ToString() {
			return $"Note: {Index}, Octave: {Octave}, Strength: {Strength}, Color: {Color}";
		}
	}

	public enum KeyColor {
		White, Black
	}
}
