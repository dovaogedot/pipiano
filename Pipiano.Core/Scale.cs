﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pipiano.Core {
	public class Scale {
		public string Name => _name;
		public IReadOnlyCollection<string> Notes => _notes;

		private string _name;
		private string[] _notes;

		public string this[int index] {
			get => _notes[index];
		}

		private Scale(string name, string[] notes) {
			if (notes.Length != 12) {
				throw new ArgumentException("Invalid number of notes provided.");
			}
			_notes = notes;
			_name = name;
		}

		public static readonly Scale CMajor = new("C Major", 
			new string[] { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" });

	}
}
