﻿using Extensions;
using Pipiano.Core;
using Pipiano.IO;
using Pipiano.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Pipiano.Tutor {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window, INotifyPropertyChanged {
		public IMidiListener MidiListener { get; set; } = new MidiListener();
		public ObservableCollection<Note> PressedNotes { get; set; } = new();
		private ObservableCollection<Note> _requiredNotes = new();
		public ObservableCollection<Note> RequiredNotes {
			get => _requiredNotes;
			set {
				_requiredNotes = value;
				this.PropChanged();
			}
		}
		public int FirstNoteIndex { get; set; } = 21;
		public int KeyCount { get; set; } = 88;

		public MainWindow() {
			DataContext = this;
			InitializeComponent();
			PressedNotes.CollectionChanged += (s, e) => {
				this.PropChanged(nameof(PressedNotes));

				var correctNotes = PressedNotes.Where(p => p.In(RequiredNotes)).ToList();

				correctNotes
					.Join(Keyboard.Keys,
						c => c.Index,
						k => k.Index,
						(c, k) => k)
					.ForEach(k => {
						k.Guess = Guess.Correct;
						//this.PropChanged(nameof(k.Guess), k);
					});

				if (RequiredNotes.All(n => n.In(PressedNotes))) {
					NextQuestion();
				}
			};

			RequiredNotes.CollectionChanged += (s, e) => this.PropChanged(nameof(RequiredNotes));

			NextQuestion();
		}

		private async Task NextQuestion() {
			await Task.Delay(2000);
			GenerateRandomNotes();
		}

		private void GenerateRandomNotes() {
			RequiredNotes.Clear();
			var rand = new Random();
			RequiredNotes.Add(new Note(rand.Next(FirstNoteIndex, KeyCount)));
			RequiredNotes.Add(new Note(rand.Next(FirstNoteIndex, KeyCount)));
			RequiredNotes.Add(new Note(rand.Next(FirstNoteIndex, KeyCount)));
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}
