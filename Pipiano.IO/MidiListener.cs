﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.Midi;

namespace Pipiano.IO {
	public class MidiListener : IMidiListener {
		public event Action<KeyPressedEvent> KeyPressed;
		public string Name { get; set; }

		private MidiInPort _midiInPort;


		public MidiListener(string name = "Default") {
			Name = name;
			Init().Wait();
		}

		~MidiListener() {
			_midiInPort.MessageReceived -= MidiInPort_MessageReceived;
		}

		private async Task Init() {
			var devices = await DeviceInformation.FindAllAsync(MidiInPort.GetDeviceSelector());
			if (devices is null || !devices.Any()) {
				throw new Exception("Could not find any MIDI device.");
			}

			_midiInPort = MidiInPort.FromIdAsync(devices[0].Id).GetResults();
			_midiInPort.MessageReceived += MidiInPort_MessageReceived;
		}

		private async void MidiInPort_MessageReceived(MidiInPort sender, MidiMessageReceivedEventArgs args) {
			var message = args.Message;
			if (message.Type == MidiMessageType.NoteOn) {
				var noteOn = (MidiNoteOnMessage) message;
				var keyPressedEvent = new KeyPressedEvent { 
					Note = noteOn.Note, Strength = noteOn.Velocity 
				};

				KeyPressed?.Invoke(keyPressedEvent);
			}
		}
	}
}
