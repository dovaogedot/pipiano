﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipiano.IO {
	public interface IMidiListener {
		event Action<KeyPressedEvent> KeyPressed;
	}

	public struct KeyPressedEvent {
		public int Note { get; init; }
		public int Strength { get; init; }

		public override string ToString() {
			return $"Note: {Note}, Strength: {Strength}";
		}
	}
}
