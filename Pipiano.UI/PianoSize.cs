﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;

namespace Pipiano.UI {
	public class PianoSize : MarkupExtension {
		public double WhiteKeyWidth { get; set; }
		public double WhiteKeyHeightMultiplier { get; set; }
		public double BlackKeyWidthMupltiplier { get; set; }
		public double BlackKeyHeightMultiplier { get; set; }

		public double WhiteKeyHeight => WhiteKeyWidth * WhiteKeyHeightMultiplier;
		public double BlackKeyWidth => WhiteKeyWidth * BlackKeyWidthMupltiplier;
		public double BlackKeyHeight => WhiteKeyHeight * BlackKeyHeightMultiplier;
		public Thickness BlackKeyMargin => new Thickness(-BlackKeyWidth / 2, 0, -BlackKeyWidth / 2, 0);

		public override object ProvideValue(IServiceProvider serviceProvider) {
			return this;
		}
	}
}
