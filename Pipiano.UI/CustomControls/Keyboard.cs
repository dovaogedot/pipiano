﻿using Extensions;
using Pipiano.Core;
using Pipiano.IO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Pipiano.UI {
	public class Keyboard : Control, INotifyPropertyChanged {
		public int KeyCount {
			get { return (int) GetValue(KeyCountProperty); }
			set { SetValue(KeyCountProperty, value); }
		}

		public int FirstKeyIndex {
			get { return (int) GetValue(FirstKeyIndexProperty); }
			set { SetValue(FirstKeyIndexProperty, value); }
		}

		public IMidiListener MidiListener {
			get { return (IMidiListener) GetValue(MidiListenerProperty); }
			set { SetValue(MidiListenerProperty, value); }
		}

		public ObservableCollection<PianoKey> Keys {
			get { return (ObservableCollection<PianoKey>) GetValue(KeysProperty); }
			set { SetValue(KeysProperty, value); }
		}

		public ObservableCollection<Note> PressedNotes {
			get { return (ObservableCollection<Note>) GetValue(PressedNotesProperty); }
			set { SetValue(PressedNotesProperty, value); }
		}

		public static readonly DependencyProperty KeyCountProperty =
			DependencyProperty.Register("KeyCount", typeof(int), typeof(Keyboard), new PropertyMetadata(88));
		public static readonly DependencyProperty FirstKeyIndexProperty =
			DependencyProperty.Register("FirstKeyIndex", typeof(int), typeof(Keyboard), new PropertyMetadata(21));
		public static readonly DependencyProperty MidiListenerProperty =
			DependencyProperty.Register("MidiListener", typeof(IMidiListener), typeof(Keyboard), new PropertyMetadata(null,	(o, e) => {
				if (e.NewValue is not IMidiListener m || o is not Keyboard k) 
					return;
				k.SetValue(MidiListenerProperty, m);
				k._keyboard.MidiListener = m;
			}));
		public static readonly DependencyProperty KeysProperty =
			DependencyProperty.Register("Keys", typeof(ObservableCollection<PianoKey>), typeof(Keyboard), new PropertyMetadata(new ObservableCollection<PianoKey>()));
		public static readonly DependencyProperty PressedNotesProperty =
			DependencyProperty.Register("PressedNotes", typeof(ObservableCollection<Note>), typeof(Keyboard), new FrameworkPropertyMetadata(null));

		public event PropertyChangedEventHandler PropertyChanged;
		
		private Core.Keyboard _keyboard;

		public Keyboard() {
			UpdateKeyboard();	
		}

		static Keyboard() {
			DefaultStyleKeyProperty.OverrideMetadata(typeof(Keyboard), new FrameworkPropertyMetadata(typeof(Keyboard)));
		}

		private void UpdateKeyboard() {
			_keyboard = new Core.Keyboard(KeyCount, FirstKeyIndex);

			_keyboard.KeyStateChanged += e => {
				Dispatcher.BeginInvoke((Action) (() => {
					var key = Keys.First(k => k.Index == e.Index);
					key.Strength = e.Strength;
					this.PropChanged(nameof(key.Strength), key);

					var note = new Note(key.Index);
					if (key.Strength == 0) {
						if (note.In(PressedNotes)) {
							PressedNotes.Remove(note);
						}
					} else {
						PressedNotes.Add(note);
					}

					var tmp = PressedNotes.ToList();
					PressedNotes.Clear();
					new ObservableCollection<Note>(
						tmp
							.GroupBy(n => n.Octave)
							.OrderBy(g => g.Key)
							.Select(g => g
								.SortLike(note.Scale.Notes, n => n.Name))
							.SelectMany(g => g)
					).ForEach(PressedNotes.Add);

					this.PropChanged(nameof(PressedNotes));
				}));
			};

			Keys = new ObservableCollection<PianoKey>(
				_keyboard.Keys.Values
					.Select(k => (PianoKey) (k.Color == KeyColor.Black ? new BlackKey(k) : new WhiteKey(k))));
		}
	}
}
