﻿using Pipiano.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Pipiano.UI {
	public class WhiteKey : PianoKey {
		static WhiteKey() {
			DefaultStyleKeyProperty.OverrideMetadata(typeof(WhiteKey), new FrameworkPropertyMetadata(typeof(WhiteKey)));
		}

		public WhiteKey(Key key) : base(key) { }
	}
}
