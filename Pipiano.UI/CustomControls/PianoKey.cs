﻿using Extensions;
using Pipiano.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Pipiano.UI {
	public abstract class PianoKey : Control, INotifyPropertyChanged {
		public int Index => _key.Index;

		public Guess Guess {
			get => _guess;
			set {
				_guess = value;
				this.PropChanged(nameof(Guess), this);
				//PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Guess)));
			}
		}
		public int Strength {
			get => _key.Strength;
			set {
				_key.Strength = value;
				this.PropChanged();
			}
		}

		private Guess _guess = Guess.Unknown;
		private Key _key;

		public event PropertyChangedEventHandler PropertyChanged;

		public PianoKey(Key key) {
			_key = key;
			DataContext = this;
			PropertyChanged += (s, e) => {

			};
		}

		static PianoKey() {
			DefaultStyleKeyProperty.OverrideMetadata(typeof(PianoKey), new FrameworkPropertyMetadata(typeof(PianoKey)));
		}
	}

	public enum Guess {
		Correct, Wrong, Unknown
	}
}
